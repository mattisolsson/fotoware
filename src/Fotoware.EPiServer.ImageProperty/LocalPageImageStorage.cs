﻿using System;
using System.IO;
using System.Net;
using System.Web;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAccess;
using EPiServer.Framework.Blobs;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using EPiServer.Web.Routing;

namespace Fotoware.EPiServer.ImageProperty
{
    /// <summary>
    /// Default implementation of Image storage
    /// </summary>
    public class LocalPageImageStorage : IImageStorage
    {
        /// <summary>
        /// content repository will be use to store Image as episerver mediaData
        /// </summary>
        private readonly IContentRepository _contentRepository;

        private readonly ContentAssetHelper _contentAssetHelper;

        public LocalPageImageStorage(IContentRepository contentRepository, ContentAssetHelper contentAssetHelper)
        {
            _contentRepository = contentRepository;
            _contentAssetHelper = contentAssetHelper;
        }

        /// <summary>
        /// Saves image as media data into epi storage by given Identification and return ID of the content
        /// </summary>
        /// <param name="imageUrl">The image URL.</param>
        /// <param name="owner">The content which owns this image (i.e. has it in its content asset folder).</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        /// <exception cref="Exception">No current context page to store image</exception>
        public ContentReference SaveToStorage(string imageUrl, ContentReference owner, string name)
        {
            if (HttpContext.Current == null)
                throw new Exception("No current context page to store image");

            //Prepare the rewuest for image
            var request = (HttpWebRequest) WebRequest.Create(imageUrl);
            var response = (HttpWebResponse) request.GetResponse();


            //Handle the response
            if ((response.StatusCode == HttpStatusCode.OK ||
                 response.StatusCode == HttpStatusCode.Moved ||
                 response.StatusCode == HttpStatusCode.Redirect) &&
                response.ContentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
            {
                //Instantionte helpers for handling the MediaData
                var contentTypeRepository = ServiceLocator.Current.GetInstance<IContentTypeRepository>();
                var mediaDataResolver = ServiceLocator.Current.GetInstance<ContentMediaResolver>();
                var blobFactory = ServiceLocator.Current.GetInstance<BlobFactory>();


                string extension = GetFileExtension(imageUrl);
                object filename = GetFileName(imageUrl);

                //Load default type for file extension
                Type mediaType = mediaDataResolver.GetFirstMatching(extension);
                //load default Epi content type for given media type
                ContentType contentType = contentTypeRepository.Load(mediaType);
                ContentAssetFolder assetFolder = _contentAssetHelper.GetOrCreateAssetFolder(owner);
                
                // If the asset folder is null, this content type is not IResourceable, and can't have it's own assets.
                if (assetFolder == null)
                    return null;

                //Create a MediaData object
                var media =
                    _contentRepository.GetDefault<MediaData>(
                        assetFolder.ContentLink ?? SiteDefinition.Current.SiteAssetsRoot, contentType.ID);
                media.Name = string.Format("{0}", string.IsNullOrEmpty(name) ? filename : name);
            
                //Gets a blob (VPP object)
                Blob blob = blobFactory.CreateBlob(media.BinaryDataContainer, extension);

                //Write image stream to blob
                using (Stream inputStream = response.GetResponseStream())
                using (Stream outputStream = blob.OpenWrite())
                {
                    var buffer = new byte[4096];
                    int bytesRead;
                    do
                    {
                        bytesRead = inputStream.Read(buffer, 0, buffer.Length);
                        outputStream.Write(buffer, 0, bytesRead);
                    } while (bytesRead != 0);
                }

                //Assign to file and publish changes
                media.BinaryData = blob;
                return _contentRepository.Save(media, SaveAction.Publish);
            }
            return ContentReference.EmptyReference;
        }

        /// <summary>
        /// Gets the URL of image by given identification. 
        /// </summary>
        /// <param name="idInStorage">The identifier in storage. ContentReference of the Media object</param>
        /// <returns>Url given by URL resolver for content reference</returns>
        public string GetUrl(ContentReference idInStorage)
        {
            try
            {
                return UrlResolver.Current.GetUrl(idInStorage);
            }
            catch (Exception)
            {
                return String.Empty;
            }
        }

        private object GetFileName(string url)
        {
            return Path.GetFileName(@url);
        }

        private string GetFileExtension(string url)
        {
            return Path.GetExtension(@url);
        }
    }
}