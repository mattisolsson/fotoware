﻿using EPiServer.Core;

namespace Fotoware.EPiServer.ImageProperty
{
    public interface IImageStorage
    {
        ContentReference SaveToStorage(string imageUrl, ContentReference owner, string name);
        string GetUrl(ContentReference idInStorage);
    }
}